import { useState } from "react";
import { Button } from "semantic-ui-react";
import PageHeader from "../components/PageHeader";
import PeopleUseEffect from "../components/useEffect/PeopleUseEffect";

export default function Home() {
  const [amount, setAmount] = useState(0);

  return (
    <>
      <PageHeader
        title="index.tsx"
        description="Fetch data with useEffect and store it with useState, inside Component."
      />
      {Array.from(Array(amount).keys()).map((i) => (
        <PeopleUseEffect key={i} />
      ))}
      <Button onClick={() => setAmount((a) => ++a)}>Add component</Button>
      <Button onClick={() => setAmount(0)}>Reset</Button>
    </>
  );
}
