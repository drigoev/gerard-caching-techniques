export const getColor = (() => {
  const colors = ["pink", "brown", "violet", "teal", "purple", "orange"];
  let index = 0;
  const cache = [];

  return (value) => {
    const inCache = cache.find((item) => item.value === value);
    if (inCache) return inCache.color;
    index = (index + 1) % colors.length;
    const color = colors[index];
    cache.push({
      value,
      color,
    });
    return color;
  };
})();
