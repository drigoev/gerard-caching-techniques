import memoize from "lodash.memoize";

const getData = () =>
  new Promise((resolve) => {
    fetch("http://localhost:3000/api/people")
      .then((response) => response.json())
      .then((data) => resolve(data));
  });

export default memoize(getData);
