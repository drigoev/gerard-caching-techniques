import { Menu, Container } from "semantic-ui-react";
import { useRouter } from "next/router";
import Link from "next/link";

export default function MainMenu() {
  const { pathname } = useRouter();

  const buttons = [
    { path: "/", text: "index.tsx" },
    { path: "/memoize", text: "memoize.tsx" },
    { path: "/context", text: "context.tsx" },
    { path: "/useMemo", text: "useMemo.tsx" },
  ];

  return (
    <Menu
      fixed="top"
      inverted
      color="blue"
      style={{ border: "5px solid white" }}
    >
      <Container fluid>
        <Menu.Item header>Caching</Menu.Item>
        {buttons.map((button) => (
          <Link key={button.text} href={button.path}>
            <Menu.Item active={pathname === button.path}>
              {button.text}
            </Menu.Item>
          </Link>
        ))}
      </Container>
    </Menu>
  );
}
